import org.junit.Assert;
import org.junit.Test;

public class MainClassTest extends MainClass

{ @Test
    public void testGetLocalNumber ()
    {
       if (getLocalNumber()==14)
           System.out.println("Возвращается корректное число - "+getLocalNumber());
       else
           System.out.println("Возвращается некорректное число - "+getLocalNumber());
    }

    @Test
    public void testGetClass_Number ()
    {   if (getClass_Number()>45)
        System.out.println("Число " + getClass_Number() + " больше 45");
        else
        System.out.println("Число " + getClass_Number() + " меньше 45");
    }

    @Test
    public void testGetClassString ()
    {   if ((getClass_String().contains("Hello")) | (getClass_String().contains("hello")))
        System.out.println("Имеется подстрока Hello или hello в слове - "+getClass_String());
    else
        Assert.fail("Тест упал, так как подстроки Hello или hello не содержатся в слове "+ getClass_String());
    }


}